<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/survey3', 'AdminController@index');
Route::get('/survey2', 'AdminController@newSurvey');

Route::resource('survey', 'NewSurveyController');

Route::get('/', function(){
	return view('surveyHtml');
});



Route::get('/success', function(){
	return view('success');
});

Route::post('getSurvey',  'AdminController@getSurvey')->name('getSurvey');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

route::get('/clear/config', function(){
		Artisan::call('config:clear');
	});

route::get('/clear/cache', function(){
	Artisan::call('cache:clear');
});


Auth::routes();
Route::middleware('auth')->prefix('admin')->group(function () {

	Route::get('/',  'AdminReports@index');

	Route::get('/reports',  'AdminReports@index');
	Route::get('/suggestions',  'AdminReports@SuggestionList');

	Route::post('suggestionTable',  'AdminReports@suggestionTable')->name('suggestionTable');

	Route::get('/annual', function(){
		return view('parts.annual');
	});

	Route::post('AnnualReport',  'AdminReports@AnnualReport')->name('AnnualReport');
	Route::post('AnnualExport',  'AdminReports@AnnualExport')->name('AnnualExport');

	Route::get('/comparison', function(){
		return view('parts.comparison');
	});

	Route::resource('questions', 'AdminQuestions');
	Route::resource('categories', 'AdminCategories');
	Route::resource('choices', 'AdminChoices');
	Route::resource('surveys', 'AdminSurvey');
	Route::resource('surveyanswers', 'AdminSurveyAnswer');

	Route::post('chartCount',  'AdminReports@chartCount')->name('chartCount');
	Route::post('chartCountComp',  'AdminComparison@chartCountComp')->name('chartCountComp');
	Route::post('chartPercent',  'AdminReports@chartPercent')->name('chartPercent');
	Route::post('chartComparisonExport',  'AdminComparison@chartComparisonExport')->name('chartComparisonExport');
	Route::post('ActiveCategory',  'AdminCategories@ActiveCategory')->name('ActiveCategory');

	Route::get('/respondents',  'AdminRespondents@index');
	Route::post('getRespondentsDetails',  'AdminRespondents@getRespondentsDetails')->name('getRespondentsDetails');

	Route::post('exportReport',  'AdminReports@exportReport')->name('exportReport');

	route::get('/export', function(){
		return view('parts.export');
	});


});





