<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Choices extends Model
{
	protected $fillable = [
       'question_id','choice_id','choice'
    ];
  	protected $dates = ['created_at','updated_at'];
    protected $table = 'choices';
    public $timestamps = true;


}
