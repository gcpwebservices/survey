<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Questions;
use App\Survey;
use App\SurveyAnswers;
use App\Choices;
use DB;
use Yajra\DataTables\DataTables;

class AdminRespondents extends Controller
{
    public function index(){
    	return view('parts.respondent');
    }

    public function getRespondentsDetails(Request $request){

     	$respondents = Survey::whereDate('survey_date', '>=',  $request->input('datefrom'))
				->whereDate('survey_date', '<=',  $request->input('dateto'))
				->latest('created_at');

 	   	return Datatables::of( $respondents )->addColumn('question1', function($event){

   			$answer = SurveyAnswers::where('survey_id', $event->id)
   				->where('question_id','1')
   				->first();
			
				if($answer->choice_id == '5'){
					return 'Excellent';
				}
				if($answer->choice_id == '4'){
					return 'Good';
				}
				if($answer->choice_id == '3'){
					return 'Average';
				}
				if($answer->choice_id == '2'){
					return 'Poor';
				}
				if($answer->choice_id == '1'){
					return 'Very poor';
				}
 	   		
        })->addColumn('question2', function($event){

 	   			$answer = SurveyAnswers::where('survey_id', $event->id) 	   			
 	   				->where('question_id','2')
 	   				->first();

 				if($answer->choice_id == '5'){
   					return 'Excellent';
   				}
				if($answer->choice_id == '4'){
   					return 'Good';
   				}
				if($answer->choice_id == '3'){
   					return 'Average';
   				}
				if($answer->choice_id == '2'){
   					return 'Poor';
   				}
				if($answer->choice_id == '1'){
   					return 'Very poor';
   				}

        })->addColumn('question3', function($event){

 	   			$answer = SurveyAnswers::where('survey_id', $event->id)	
 	   				->where('question_id','3')
 	   				->first();

 				if($answer->choice_id == '5'){
   					return 'Excellent';
   				}
				if($answer->choice_id == '4'){
   					return 'Good';
   				}
				if($answer->choice_id == '3'){
   					return 'Average';
   				}
				if($answer->choice_id == '2'){
   					return 'Poor';
   				}
				if($answer->choice_id == '1'){
   					return 'Very poor';
   				}
 	   	
        })->addColumn('question4', function($event){

 	   			$answer = SurveyAnswers::where('survey_id', $event->id)	
 	   				->where('question_id','4')
 	   				->first();

 				if($answer->choice_id == '5'){
   					return 'Excellent';
   				}
				if($answer->choice_id == '4'){
   					return 'Good';
   				}
				if($answer->choice_id == '3'){
   					return 'Average';
   				}
				if($answer->choice_id == '2'){
   					return 'Poor';
   				}
				if($answer->choice_id == '1'){
   					return 'Very poor';
   				}
 	   			
        })->addColumn('question5', function($event){

 	   			$answer = SurveyAnswers::where('survey_id', $event->id)
 	   				->where('question_id','5')
 	   				->first();

 				if($answer->choice_id == '5'){
   					return 'Excellent';
   				}
				if($answer->choice_id == '4'){
   					return 'Good';
   				}
				if($answer->choice_id == '3'){
   					return 'Average';
   				}
				if($answer->choice_id == '2'){
   					return 'Poor';
   				}
				if($answer->choice_id == '1'){
   					return 'Very poor';
   				}
 	   			
        })->editColumn('created_at', function($event){
 	   		return $event->created_at->format('l j F Y h:i A');

        })->rawColumns(['respondent','company','mobile','suggestion','created_at','question1','question2',
        'question3','question4','question5','suggestion'])->make( true );
    }
}
