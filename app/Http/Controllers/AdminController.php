<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questions;
use App\SurveyAnswers;
use App\Survey;
use App\Categories;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
	public function index()
	{	
		$category = Categories::where('is_active',1)->with(['getQuestions'])->get();
		$data['survey'] = $category;
		return view('survey',$data);
	}

	public function newSurvey(){

		$category = Categories::where('is_active',1)->with(['getQuestions'])->get();
		$data['survey'] = $category;
		return view('survey2',$data);
	}

	public function getSurvey(Request $request)
	{
		$category = Categories::where('is_active',1)->first();
		$all = $request->input('arr');

		if(array_key_exists("suggestions", $all)){
			$arr_suggest = $all['suggestions'];
		}else{
			$arr_suggest = '';
		}

    	DB::beginTransaction();
	  	try
	        {
				$survey_id = Survey::create([
					'category_id' => $category->id,
					'respondent'  => $all['respondentdetails']['respondent'],
					'company'     => $all['respondentdetails']['company'],
					'mobile'      => $all['respondentdetails']['mobile'],
					'suggestion'  => $arr_suggest,
					'survey_location' => '1',
					'survey_date' => date('Y-m-d')
				]);

				$surveys = [];
				foreach($all['Quality'] as $key => $value){

					$survey = [
						'question_id' => $key,
						'choice_id'  => $value,
			
					];

					$survey_id->getSurveyAnswers()->create($survey);
				}
	            DB::commit();
	        }

 		catch (Exception $e)
            {
                DB::rollBack();
                throw $e;
            }
	}


	
}

