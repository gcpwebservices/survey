<?php

namespace App\Http\Controllers;

use App\Choices;
use Illuminate\Http\Request;
use App\Questions;

class AdminChoices extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = Questions::all();
        $data['questions'] = $all;
        return view('parts.choices',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question_id = $request->input('question_id');
        $choice1     = $request->input('choice1');
        $choice2     = $request->input('choice2');
        $choice3     = $request->input('choice3');
        $choice4     = $request->input('choice4');
        $choice5     = $request->input('choice5');

        $arr = [
            'question_id' => $question_id,
            'choice1'     => $choice1,
            'choice2'     => $choice2,
            'choice3'     => $choice3,
            'choice4'     => $choice4,
            'choice5'     => $choice5
        ];

        Choices::create($arr);
        return redirect()->route('choices.index')->with('status',['success','New Choices are created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Choices  $choices
     * @return \Illuminate\Http\Response
     */
    public function show(Choices $choices)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Choices  $choices
     * @return \Illuminate\Http\Response
     */
    public function edit(Choices $choices)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Choices  $choices
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Choices $choices)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Choices  $choices
     * @return \Illuminate\Http\Response
     */
    public function destroy(Choices $choices)
    {
        //
    }
}
