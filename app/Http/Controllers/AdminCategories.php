<?php

namespace App\Http\Controllers;
use App\Categories;
use Illuminate\Http\Request;

class AdminCategories extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['categories'] = Categories::get();
        return view('parts.categories',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('parts.add-category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category     = $request->input('category');
        $description  = $request->input('description');
        $active_from  = $request->input('active_from');
        $active_to    = $request->input('active_to');
  
        $arr = [

            'category'    => $category,
            'description' => $description,
            'is_active'   => 0,
            'active_from' => date('Y-m-d',strtotime($active_from)),
            'active_to'   => date('Y-m-d',strtotime($active_to))

        ];

        Categories::create($arr);
        return redirect()->route('categories.index')->with('status',['success','New Category is created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function ActiveCategory(Request $request)
    {
        $category_id = $request->input('category_id');

        Categories::where('is_active', '=','1')->update(['is_active' => 0]);

        $category = Categories::where('id',$category_id)->first();
        $category->is_active = '1';
        $category->save();
 
        return redirect()->route('categories.index')->with('status',['success','Category is activated now']);
    }
}
