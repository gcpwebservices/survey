<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questions;
use App\Survey;
use App\SurveyAnswers;
use App\Choices;
use DB;

use App\Exports\ExportData;
use Maatwebsite\Excel\Facades\Excel;

class AdminComparison extends Controller
{
    public function index()
    {
    	return view('parts.comparison');
    }

    public function chartCountComp(Request $request)
    {
        $datefrom = $request->input('datefrom');
        $dateto = $request->input('dateto');


        $choices = Choices::get();
    	$AllQuestion  =[
		    '1' => [0,0,0,0,0],
  			'2' => [0,0,0,0,0],
  			'3' => [0,0,0,0,0],
  			'4' => [0,0,0,0,0],
  			'5' => [0,0,0,0,0]
  		];


        foreach( $choices as $choice ) 
        {
	        $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
						FROM questions AS q
						LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
						LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
						LEFT JOIN survey as s on sa.survey_id = s.id
						WHERE sa.choice_id =  '".$choice->choice_id."'
						AND q.category_id = '2'
						AND s.survey_date >= '".$datefrom."'
						AND s.survey_date <= '".$dateto."'
						GROUP BY sa.question_id
						ORDER BY q.id");

	        foreach($All as $ulol=> $per_question) {
				    $AllQuestion[$choice->choice_id][$per_question->id-1] = intval($per_question->total);
				  }

        }


		return json_encode($AllQuestion);

  }


  public function chartPercentComp(Request $request)
  {
        $datefrom = $request->input('datefrom');
        $dateto = $request->input('dateto');


        $choices = Choices::get();
      $AllQuestion  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

      $percent  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];
        foreach( $choices as $choice ) 
        {
          $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
            FROM questions AS q
            LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
            LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
            LEFT JOIN survey as s on sa.survey_id = s.id
            WHERE sa.choice_id =  '".$choice->choice_id."'
            AND q.category_id = '2'
            AND s.survey_date >= '".$datefrom."'
            AND s.survey_date <= '".$dateto."'
            GROUP BY sa.question_id
            ORDER BY q.id");

          foreach($All as $ulol=> $per_question) {
            $AllQuestion[$choice->choice_id][$per_question->id-1] = $per_question->total;
          }

        }

        $total=[
          1 => 0,
          2 => 0,
          3 => 0,
          4 => 0,
          5 => 0,
        ];
        foreach($AllQuestion as $key=>$value){
            foreach($value as $key2 => $value2){
              $total[$key2 + 1] += $value2;

            }
        }

        foreach($AllQuestion as $key =>$newVal){
          foreach($newVal as $key2 => $newVal2){
            $qtotal = $total[$key2 + 1];
            if($newVal2 != 0) {
              $percent[$key][$key2] = round(($newVal2 / $qtotal) * 100,2);
            }else{
              $percent[$key][$key2] = 0;
            }
           
          }
        }

    return json_encode($percent);

  }


  public function chartComparisonExport(Request $request){

    $datefrom1 = $request->input('datefrom1');
    $dateto1 = $request->input('dateto1');

    $datefrom2 = $request->input('datefrom2');
    $dateto2 = $request->input('dateto2');

        $choices = Choices::get();
        $allQ = Questions::where('category_id',2)->get();

      $AllQuestion1  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

      $AllQuestion2  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

        foreach( $choices as $choice ) 
        {
            $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
            FROM questions AS q
            LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
            LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
            LEFT JOIN survey AS s ON sa.survey_id = s.id
            WHERE sa.choice_id =  '".$choice->choice_id."'
            AND q.category_id = '2'
            AND s.survey_date >= '".$datefrom1."'
            AND s.survey_date <= '".$dateto1."'
            GROUP BY sa.question_id
            ORDER BY q.id");

            foreach($All as $ulol=> $per_question) {
               $AllQuestion1[$choice->choice_id][$per_question->id-1] = intval($per_question->total);
            }
        }

        foreach( $choices as $choice ) 
        {
            $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
            FROM questions AS q
            LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
            LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
            LEFT JOIN survey AS s ON sa.survey_id = s.id
            WHERE sa.choice_id =  '".$choice->choice_id."'
            AND q.category_id = '2'
            AND s.survey_date >= '".$datefrom2."'
            AND s.survey_date <= '".$dateto2."'
            GROUP BY sa.question_id
            ORDER BY q.id");

            foreach($All as $ulol=> $per_question) {
               $AllQuestion2[$choice->choice_id][$per_question->id-1] = intval($per_question->total);
            }
        }

        $sug = '';
        $data['questions'] = $allQ;
        $data['answers']  = $AllQuestion1;
        $data['answers2']  = $AllQuestion2;
        $data['suggestions'] =$sug;
        $data['fromto'] = 'Count: '.date('F d Y', strtotime($datefrom1))." - ".date('F d Y', strtotime($dateto1));
        $data['fromto2'] = 'Count: '.date('F d Y', strtotime($datefrom2))." - ".date('F d Y', strtotime($dateto2));

      return Excel::download( new ExportData($data), 'Comparison'.'.xlsx');
  }


}
