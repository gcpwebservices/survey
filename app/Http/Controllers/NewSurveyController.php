<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questions;
use App\SurveyAnswers;
use App\Survey;
use App\Categories;
use Illuminate\Support\Facades\DB;

class NewSurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('surveyHtml');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fullname'=>'required',
            'company'=> 'required',
            'mobile' => 'numeric',
            'q1' => 'required',
            'q2' => 'required',
            'q3' => 'required',
            'q4' => 'required',
            'q5' => 'required'
        ]);

        $category = Categories::where('is_active',1)->first();
        DB::beginTransaction();
        try
            {
                $survey_id = Survey::create([
                    'category_id' => $category->id,
                    'respondent'  => $request->input('fullname'),
                    'company'     => $request->input('company'),
                    'mobile'      => $request->input('mobile'),
                    'suggestion'  => $request->input('suggestions'),
                    'survey_location' => '1',
                    'survey_date' => date('Y-m-d')
                ]);

                $surveys = [

                    '1' => $request->input('q1'),
                    '2' => $request->input('q2'),
                    '3' => $request->input('q3'),
                    '4' => $request->input('q4'),
                    '5' => $request->input('q5'),

                ];

                $surveyAnswersArray =[];
                foreach( $surveys as $key => $value){

                    $surveyAnswersArray = [
                        'question_id' => $key,
                        'choice_id'  => $value,
            
                    ];

                    $survey_id->getSurveyAnswers()->create($surveyAnswersArray);
                }
                DB::commit();
            }

        catch (Exception $e)
            {
                DB::rollBack();
                throw $e;
            }
            
        return redirect('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
