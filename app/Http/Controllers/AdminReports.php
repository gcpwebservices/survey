<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questions;
use App\Survey;
use App\SurveyAnswers;
use App\Choices;
use DB;

use App\Exports\ExportData;
use Maatwebsite\Excel\Facades\Excel;

class AdminReports extends Controller
{
    public function index()
    {
      return view('parts.reports');
    }

    public function SuggestionList(){
        $data['data'] = Survey::get();
        return view('parts.suggestions' ,$data);
    }

    public function suggestionTable(Request $request){

      $datefrom = $request->input('datefrom');
      $dateto = $request->input('dateto');

      $data['data'] = Survey::where('suggestion','!=','')
                            ->whereDate ('survey_date','>=',$datefrom)
                            ->whereDate ('survey_date','<=',$dateto)
                            ->get();
      return json_encode($data);

    }

    public function chartCount(Request $request)
    {
      $datefrom = $request->input('datefrom');
      $dateto = $request->input('dateto');


      $choices = Choices::get();
      $AllQuestion  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

        foreach( $choices as $choice ) 
        {
          $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
            FROM questions AS q
            LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
            LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
            LEFT JOIN survey as s on sa.survey_id = s.id
            WHERE sa.choice_id =  '".$choice->choice_id."'
            AND q.category_id = '2'
            AND s.survey_date >= '".$datefrom."'
            AND s.survey_date <= '".$dateto."'
            GROUP BY sa.question_id
            ORDER BY q.id");

          foreach($All as $ulol=> $per_question) {
            $AllQuestion[$choice->choice_id][$per_question->id-1] = intval($per_question->total);
          }

        }

    return json_encode($AllQuestion);

  }


  public function chartPercent(Request $request)
  {
        $datefrom = $request->input('datefrom');
        $dateto = $request->input('dateto');


      $choices = Choices::get();
      $AllQuestion  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

      $percent  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];
        foreach( $choices as $choice ) 
        {
          $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
            FROM questions AS q
            LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
            LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
            LEFT JOIN survey as s on sa.survey_id = s.id
            WHERE sa.choice_id =  '".$choice->choice_id."'
            AND q.category_id = '2'
            AND s.survey_date >= '".$datefrom."'
            AND s.survey_date <= '".$dateto."'
            GROUP BY sa.question_id
            ORDER BY q.id");

          foreach($All as $ulol=> $per_question) {
            $AllQuestion[$choice->choice_id][$per_question->id-1] = $per_question->total;
          }

        }

        $total=[
          1 => 0,
          2 => 0,
          3 => 0,
          4 => 0,
          5 => 0,
        ];
        foreach($AllQuestion as $key=>$value){
            foreach($value as $key2 => $value2){
              $total[$key2 + 1] += $value2;

            }
        }

        foreach($AllQuestion as $key =>$newVal){
          foreach($newVal as $key2 => $newVal2){
            $qtotal = $total[$key2 + 1];
            if($newVal2 != 0) {
              $percent[$key][$key2] = round(($newVal2 / $qtotal) * 100,2);
            }else{
              $percent[$key][$key2] = 0;
            }
           
          }
        }

    return json_encode($percent);

  }

  public function exportReport(Request $request){

    $datefrom = ($request->input('datefrom') == '') ? date('Y-m-d', strtodate('-7 days')) : $request->input('datefrom');
    $dateto = ($request->input('dateto') == '') ? date('Y-m-d', strtodate(now())) : $request->input('dateto');

    $sug = Survey::where('survey_date', '>=',$datefrom)
          ->where('survey_date', '<=', $dateto)
          ->where('suggestion', '!=', '')
          ->get();

        $choices = Choices::get();
        $allQ = Questions::where('category_id',2)->get();

      $AllQuestion  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

      $AllQuestionE  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

      $percent  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

        foreach( $choices as $choice ) 
        {
              $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
            FROM questions AS q
            LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
            LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
            LEFT JOIN survey AS s ON sa.survey_id = s.id
            WHERE sa.choice_id =  '".$choice->choice_id."'
            AND q.category_id = '2'
            AND s.survey_date >= '".$datefrom."'
            AND s.survey_date <= '".$dateto."'
            GROUP BY sa.question_id
            ORDER BY q.id");

            foreach($All as $ulol=> $per_question) {
               $AllQuestion[$choice->choice_id][$per_question->id-1] = intval($per_question->total);
            }
        }



        foreach( $choices as $choiceE ) 
        {
          $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
            FROM questions AS q
            LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
            LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
            LEFT JOIN survey as s on sa.survey_id = s.id
            WHERE sa.choice_id =  '".$choiceE->choice_id."'
            AND q.category_id = '2'
            AND s.survey_date >= '".$datefrom."'
            AND s.survey_date <= '".$dateto."'
            GROUP BY sa.question_id
            ORDER BY q.id");

          foreach($All as $ulol=> $per_question) {
            $AllQuestionE[$choiceE->choice_id][$per_question->id-1] = $per_question->total;
          }

        }

        $total=[
          1 => 0,
          2 => 0,
          3 => 0,
          4 => 0,
          5 => 0,
        ];
        foreach($AllQuestionE as $key=>$value){
            foreach($value as $key2 => $value2){
              $total[$key2 + 1] += $value2;

            }
        }

        foreach($AllQuestionE as $key =>$newVal){
          foreach($newVal as $key2 => $newVal2){
            $qtotal = $total[$key2 + 1];
            if($newVal2 != 0) {
              $percent[$key][$key2] = round(($newVal2 / $qtotal) * 100,2);
            }else{
              $percent[$key][$key2] = 0;
            }
           
          }
        }


        $data['questions'] = $allQ;
        $data['answers']  = $AllQuestion;
        $data['answers2']  = $percent;
        $data['suggestions'] =$sug;
        $data['fromto'] = 'Count: '.date('F d Y', strtotime($datefrom))." - ".date('F d Y', strtotime($dateto));
        $data['fromto2'] = 'Percentage: '.date('F d Y', strtotime($datefrom))." - ".date('F d Y', strtotime($dateto));

      return Excel::download( new ExportData($data), 'Count & Percentage'.'.xlsx');

  }


  public function AnnualReport(Request $request)
  {
      $year = $request->input('dateYear');

      $choices = Choices::get();
      $AllQuestion  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

      $percent  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];
        foreach( $choices as $choice ) 
        {
          $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
            FROM questions AS q
            LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
            LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
            LEFT JOIN survey as s on sa.survey_id = s.id
            WHERE sa.choice_id =  '".$choice->choice_id."'
            AND q.category_id = '2'
            AND YEAR(s.survey_date) = '".$year."'
            GROUP BY sa.question_id
            ORDER BY q.id");

          foreach($All as $ulol=> $per_question) {
            $AllQuestion[$choice->choice_id][$per_question->id-1] = intval($per_question->total);
          }

        }


    return json_encode($AllQuestion);

  }

  public function AnnualExport(Request $request)
  {
    $year = $request->input('my_hidden_input');

    $sug = Survey::whereYear('survey_date', '=',$year)->get();

        $choices = Choices::get();
        $allQ = Questions::where('category_id',2)->get();

       $AllQuestion  =[
        '1' => [0,0,0,0,0],
        '2' => [0,0,0,0,0],
        '3' => [0,0,0,0,0],
        '4' => [0,0,0,0,0],
        '5' => [0,0,0,0,0]
      ];

        foreach( $choices as $choice ) 
        {
              $All = DB::SELECT("SELECT q.id, q.question, COUNT(sa.choice_id) AS total, c.choice
            FROM questions AS q
            LEFT JOIN survey_answers AS sa ON q.id = sa.question_id
            LEFT JOIN choices AS c ON c.choice_id = sa.choice_id
            LEFT JOIN survey AS s ON sa.survey_id = s.id
            WHERE sa.choice_id =  '".$choice->choice_id."'
            AND q.category_id = '2'
            AND YEAR(s.survey_date) = '".$year."'
            GROUP BY sa.question_id
            ORDER BY q.id");

            foreach($All as $ulol=> $per_question) {
               $AllQuestion[$choice->choice_id][$per_question->id-1] = $per_question->total;
            }
        }

        $data['questions'] = $allQ;
        $data['answers']  = $AllQuestion;
        $data['suggestions'] =$sug;
        $data['fromto'] = 'Report for Year: '.date('Y', strtotime($year));

      return Excel::download( new ExportData($data), 'Annual'.'.xlsx');
  }


}
