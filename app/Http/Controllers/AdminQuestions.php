<?php

namespace App\Http\Controllers;

use App\Questions;
use App\Categories;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AdminQuestions extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data['cat'] = Categories::get();

        return view('parts.add-questions',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $question     = $request->input('question');
        $category     = $request->input('category_id');
        $description  = $request->input('description');
        $active_from  = $request->input('active_from');
        $active_to    = $request->input('active_to');
  
        $arr = [

            'question'    => $question,
            'category_id' => $category,
            'description' => $description,
            'active_from' => date('Y-m-d',strtotime($active_from)),
            'active_to'   => date('Y-m-d',strtotime($active_to))

        ];

        Questions::create($arr);
        return redirect()->route('questions.index')->with('status',['success','New Question is created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function show(Questions $questions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function edit(Questions $questions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questions $questions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Questions  $questions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questions $questions)
    {
        //
    }
}
