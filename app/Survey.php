<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
 	protected $fillable = [
       'respondent','company','mobile','category_id','suggestion','survey_location','survey_date'
    ];
  	protected $dates = ['survey_date','created_at','updated_at'];
    protected $table = 'survey';
    public $timestamps = true;

    public function getSurveyAnswers(){
    	return $this->hasMany('App\SurveyAnswers','survey_id','id');
    }


    
}
