<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
	protected $fillable = [
       'category_id','description','is_active','active_from','active_to'
    ];
  	protected $dates = ['active_from','active_to','created_at','updated_at'];
    protected $table = 'category';
    public $timestamps = true;

    public function getQuestions(){
    	return $this->hasMany('App\Questions','category_id','id');
    }
}
