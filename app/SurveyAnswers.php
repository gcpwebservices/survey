<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyAnswers extends Model
{
 	protected $fillable = [
       'survey_id','question_id','choice_id'
    ];
  	protected $dates = ['created_at','updated_at'];
    protected $table = 'survey_answers';
    public $timestamps = true;

    public function getSurvey(){
    	return $this->belongsTo('App\Survey','survey_id','id');
    }

    public function getChoicesName(){
		return $this->belongsTo('App\Choices','choice_id','choice_id');
    }

}
