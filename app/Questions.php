<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
	protected $fillable = [
       'category_id','question','description','active_from','active_to'
    ];
  	protected $dates = ['active_from','active_to','created_at','updated_at'];
    protected $table = 'questions';
    public $timestamps = true;

   	public function getActiveCategory(){
    	return $this->belongsTo('App\Categories','category','id');
    }

    public function getSurveyAnswers(){
      return $this->hasMany('App\SurveyAnswers','question_id','id');
    }

}
