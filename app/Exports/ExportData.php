<?php

namespace App\Exports;

use App\Categories;
use App\Questions;
use App\Survey;
use App\SurveyAnswers;
use App\Choices;

// use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportData implements FromView
{
	public $allexport;

	public function __construct($all = '')
	{
		$this->allexport = $all;
	}

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
      	return view('parts.export',$this->allexport);
    }
}
