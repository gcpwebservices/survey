@extends('layouts.app')

@section('content-styles')
    <link rel="stylesheet" href="{{ asset('template/vendor/fontawesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/metisMenu/dist/metisMenu.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/animate.css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap/dist/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/select2-3.5.2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/select2-bootstrap/select2-bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/clockpicker/dist/bootstrap-clockpicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">
    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">

<style type="text/css">


   
    .color-line {
        background-color: rgba(128,38,43,0.8) !important;
        background-image: none;
        height: 3px;
    }


 


</style>
@endsection

@section('content')
<body id="body">
    <main class="">
        <div id="logo1" class="col-sm-12 text-center">
            <img class="img-fluid" alt="Responsive image" width="180" src="{{ asset('images/raktd.png') }}">
            <div>
                <h4 style="color:#6d7072;">آراؤكم تهمنا - إدارة التراخيص السياحية و ضمان الجودة</h4>
                <h4 style="color: #6d7072;">We Value your opinions - TLQA Department </h4>
            </div>
            <div class="color-line"></div>
        </div> 
        <div class="container-fluid text-center" style="padding: 50px;">
            <h1>Thank you for completing our survey!</h1>
        </div>
    </main>
</body>

@endsection

@section('content-scripts')
<script src="{{ asset('template/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('template/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('template/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('template/vendor/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>
<script src="{{ asset('template/vendor/moment/moment.js') }}"></script>
<script src="{{ asset('template/vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
<script src="{{ asset('template/vendor/select2-3.5.2/select2.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>

<script type="text/javascript">

$(document).ready(function(){
    var url = "{{ url('survey')}}"
    setTimeout(function(){ 
       window.location.replace(url);
    }, 3000);
});

</script>
@endsection