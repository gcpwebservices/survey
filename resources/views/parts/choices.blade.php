@extends('layouts.app')

@section('content-styles')

    <link rel="stylesheet" href="{{ asset('template/vendor/fontawesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/metisMenu/dist/metisMenu.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/animate.css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap/dist/css/bootstrap.css') }}" />

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">

    <style type="text/css">
        .color-line {
            background-color: rgba(128,38,43,0.8) !important;
            background-image: none;
        }
    </style>

@endsection

@section('content')

@include('layouts.header')
<div id="wrapper">

    <div class="content animate-panel">
        @if(session('status'))
        <div class="col-lg-12" id="successMessage" >
                <div class="hpanel">
                    <div class="panel-body">
                      
                        <div class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">
                        {{session('status')[1]}}
                        </div>
              
                    </div>
                </div>
            </div>
        @endif



        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        Add Choices
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('choices.store') }}" >
                        @csrf
                            <div class="form-group"><label class="col-sm-2 control-label">Question Name</label>
                                <div class="col-sm-10">
                                    <div class="col-md-5">
                                        <select class="form-control m-b" name="question_id">
                                            <option>Select</option>
                                            @foreach($questions as $question)
                                                <option value="{{$question->id}}">{{$question->question}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Choice Name #1</label>

                                <div class="col-sm-10">
                                    <div class="col-md-4">
                                        <input type="text" placeholder="" class="form-control" name="choice1">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Choice Name #2</label>

                                <div class="col-sm-10">
                                    <div class="col-md-4">
                                        <input type="text" placeholder="" class="form-control" name="choice2">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Choice Name #3</label>

                                <div class="col-sm-10">
                                    <div class="col-md-4">
                                        <input type="text" placeholder="" class="form-control" name="choice3">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Choice Name #4</label>

                                <div class="col-sm-10">
                                    <div class="col-md-4">
                                        <input type="text" placeholder="" class="form-control" name="choice4">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Choice Name #5</label>

                                <div class="col-sm-10">
                                    <div class="col-md-4">
                                        <input type="text" placeholder="" class="form-control" name="choice5">
                                    </div>
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
            
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <a href="{{ route('choices.index') }}" class="btn btn-default">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Save changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('layouts.footer')
</div>



@endsection


@section('content-scripts')

<script src="{{ asset('template/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('template/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('template/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('template/vendor/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>

<script src="{{ asset('template/vendor/moment/moment.js') }}"></script>

<script src="{{ asset('template/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<!-- App scripts -->
<script src="{{ asset('template/scripts/homer.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#active_from, #active_to').datepicker();

        setTimeout(function() {
            $('#successMessage').fadeOut('fast');
        }, 5000);
        
    });
</script>
@endsection