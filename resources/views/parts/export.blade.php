@extends('layouts.app')



@section('content-styles')



    <link rel="stylesheet" href="{{ asset('template/vendor/fontawesome/css/font-awesome.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/vendor/metisMenu/dist/metisMenu.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/vendor/animate.css/animate.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap/dist/css/bootstrap.css') }}" />



    <!-- App styles -->

    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />



    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/helper.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">







@endsection



@section('content')





<div class="container">

    <div class="table-responsive">

       <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">

            <tr>

                <th>{{ $fromto }}</th>          

            </tr>

        </table>



            <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">



                <tr> 

                    <td>Questions</td>

                    <td>Excellent</td>

                    <td>Good</td>

                    <td>Average</td>

                    <td>Poor</td>

                    <td>Very poor</td>

                </tr>

   

             

                <tr>

                    <td>{{$questions['0']['question']}}</td>

                    <td>{{ $answers['5']['0'] }}</td>

                    <td>{{ $answers['5']['1'] }}</td>

                    <td>{{ $answers['5']['2'] }}</td>

                    <td>{{ $answers['5']['3'] }}</td>

                    <td>{{ $answers['5']['4'] }}</td>

                </tr>



                <tr>

                    <td>{{$questions['1']['question']}}</td>

                    <td>{{ $answers['4']['0'] }}</td>

                    <td>{{ $answers['4']['1'] }}</td>

                    <td>{{ $answers['4']['2'] }}</td>

                    <td>{{ $answers['4']['3'] }}</td>

                    <td>{{ $answers['4']['4'] }}</td>

                </tr>



               <tr>

                    <td>{{$questions['2']['question']}}</td>

                    <td>{{ $answers['3']['0'] }}</td>

                    <td>{{ $answers['3']['1'] }}</td>

                    <td>{{ $answers['3']['2'] }}</td>

                    <td>{{ $answers['3']['3'] }}</td>

                    <td>{{ $answers['3']['4'] }}</td>

                </tr>



                <tr>

                    <td>{{$questions['3']['question']}}</td>

                    <td>{{ $answers['2']['0'] }}</td>

                    <td>{{ $answers['2']['1'] }}</td>

                    <td>{{ $answers['2']['2'] }}</td>

                    <td>{{ $answers['2']['3'] }}</td>

                    <td>{{ $answers['2']['4'] }}</td>

                </tr>



                <tr>

                    <td>{{$questions['4']['question']}}</td>

                    <td>{{ $answers['1']['0'] }}</td>

                    <td>{{ $answers['1']['1'] }}</td>

                    <td>{{ $answers['1']['2'] }}</td>

                    <td>{{ $answers['1']['3'] }}</td>

                    <td>{{ $answers['1']['4'] }}</td>

                </tr>

  

     

    

        </table>


       <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">

            <tr>

                <th>{{ $fromto2 }}</th>          

            </tr>

        </table>


        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">



                <tr> 

                    <td>Questions</td>

                    <td>Excellent</td>

                    <td>Good</td>

                    <td>Average</td>

                    <td>Poor</td>

                    <td>Very poor</td>

                </tr>

   

             

                <tr>

                    <td>{{$questions['0']['question']}}</td>

                    <td>{{ $answers2['5']['0'] }}</td>

                    <td>{{ $answers2['5']['1'] }}</td>

                    <td>{{ $answers2['5']['2'] }}</td>

                    <td>{{ $answers2['5']['3'] }}</td>

                    <td>{{ $answers2['5']['4'] }}</td>

                </tr>



                <tr>

                    <td>{{$questions['1']['question']}}</td>

                    <td>{{ $answers2['4']['0'] }}</td>

                    <td>{{ $answers2['4']['1'] }}</td>

                    <td>{{ $answers2['4']['2'] }}</td>

                    <td>{{ $answers2['4']['3'] }}</td>

                    <td>{{ $answers2['4']['4'] }}</td>

                </tr>



               <tr>

                    <td>{{$questions['2']['question']}}</td>

                    <td>{{ $answers2['3']['0'] }}</td>

                    <td>{{ $answers2['3']['1'] }}</td>

                    <td>{{ $answers2['3']['2'] }}</td>

                    <td>{{ $answers2['3']['3'] }}</td>

                    <td>{{ $answers2['3']['4'] }}</td>

                </tr>



                <tr>

                    <td>{{$questions['3']['question']}}</td>

                    <td>{{ $answers2['2']['0'] }}</td>

                    <td>{{ $answers2['2']['1'] }}</td>

                    <td>{{ $answers2['2']['2'] }}</td>

                    <td>{{ $answers2['2']['3'] }}</td>

                    <td>{{ $answers2['2']['4'] }}</td>

                </tr>



                <tr>

                    <td>{{$questions['4']['question']}}</td>

                    <td>{{ $answers2['1']['0'] }}</td>

                    <td>{{ $answers2['1']['1'] }}</td>

                    <td>{{ $answers2['1']['2'] }}</td>

                    <td>{{ $answers2['1']['3'] }}</td>

                    <td>{{ $answers2['1']['4'] }}</td>

                </tr>

  

     

    

        </table>


    </div>



    <div class="table-responsive">

        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">

            <thead>

            <tr>
                @if(!empty($suggestions))
                    <th>Suggestions</th>
                @endif
            </tr>

            </thead>

            <tbody>
                @if(!empty($suggestions))
                    @foreach($suggestions as $suggestion)

                    <tr>

                        <td>{{ $suggestion->suggestion }}</td>

                    </tr>

                    @endforeach
                @endif
            </tbody>

        </table>

    </div>

</div>







@endsection





@section('content-scripts')



<script src="{{ asset('template/vendor/jquery/dist/jquery.min.js') }}"></script>

<script src="{{ asset('template/vendor/jquery-ui/jquery-ui.min.js') }}"></script>

<script src="{{ asset('template/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>

<script src="{{ asset('template/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('template/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>

<script src="{{ asset('template/vendor/iCheck/icheck.min.js') }}"></script>

<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>



<script src="{{ asset('template/vendor/moment/moment.js') }}"></script>



<!-- App scripts -->

<script src="{{ asset('template/scripts/homer.js') }}"></script>

<script type="text/javascript">



</script>

@endsection