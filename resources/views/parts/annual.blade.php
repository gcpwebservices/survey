@extends('layouts.app')



@section('content-styles')


    <link rel="stylesheet" href="{{ asset('template/vendor/fontawesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/metisMenu/dist/metisMenu.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/animate.css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap/dist/css/bootstrap.css') }}" />

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">


    <style type="text/css">

        .color-line {

            background-color: rgba(128,38,43,0.8) !important;

            background-image: none;

        }



        .highcharts-container, .highcharts-container svg { width: 100% !important; }



    </style>



@endsection



@section('content')



@include('layouts.header')

<div id="wrapper">

    <div class="content">

        <div class="row">

            <div class="col-sm-12 col-lg-6">

                <div class="hpanel">

                    <div class="panel-heading">

                        <div class="panel-tools">

                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>

                        </div>

                        Action

                    </div>

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-sm-12">

          

                                 <div class="col-sm-8 col-md-8">

                                    <strong>Date Filter</strong>

                                    <div class="input-group">

                                        <input class="form-control datefilter" name="datefilter" type="text" placeholder="" autocomplete="off">

                                        <div class="input-group-btn">

                                            <button class="btn btn-default datefilter"><i class="fa fa-calendar"></i></button>

                                        </div>

                                    </div>

                                </div>  

     

                                <div class="col-sm-4 col-md-4">

                                    <strong>Export Excel</strong>

                                    <div class="form-group">

                                        <form method="POST" enctype="multipart/form-data" action="{{ route('AnnualExport')}}">

                                        @csrf

                                            <input type="hidden" name="my_hidden_input" id="my_hidden_input">

                                            <button class="btn btn-default" type="submit"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>

                                        </form>

                                    </div>

                                </div>    

                         

                            </div>



                        </div>

                    </div>

                </div>

            </div>



        </div>



        <div class="row">

            <div class="col-sm-12 col-md-12">

                <div class="hpanel">

                    <div class="panel-heading">

                        <div class="panel-tools">

                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>

                        </div>

                       Annual count of the score for each category

                    </div>

                    <div class="panel-body">

                        <div id="canvasCount" style="width: 100%;"></div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@include('layouts.footer')

</div>







@endsection





@section('content-scripts')



<script src="{{ asset('template/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('template/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('template/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('template/vendor/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>

<script src="{{ asset('template/vendor/moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/vendor/daterangepicker/moment.min.js') }}"></script>

<script src="{{ asset('template/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="{{ asset('template/highcharts/highcharts.js') }}"></script>
<script src="{{ asset('template/highcharts/exporting.js') }}"></script>
<script src="{{ asset('template/highcharts/export-data.js') }}"></script>

<script src="{{ asset('template/scripts/homer.js') }}"></script>

<script type="text/javascript">

$(document).ready(function(){



    var myChartCount = null;



    $('.datefilter').datepicker({

        minViewMode: 2,

        format: 'yyyy',

        startDate: moment().format('YYYY')

    });



    $('.datefilter').val(moment().format('YYYY'));

    $('#my_hidden_input').val(moment().format('YYYY'));



    $.ajax({

        url: '{{ route('AnnualReport') }}',

        type: 'POST',

        data: {

            _token  : $('meta[name="csrf-token"]').attr('content'),

            dateYear : $('#my_hidden_input').val(),

        

        }, 

        success:function(response){

            var obj = JSON.parse(response);

            drawChartCount(obj,$('#my_hidden_input').val());

        }



    });





    $(".datefilter").on("changeDate", function(event) {

        $("#my_hidden_input").val(

            $(".datefilter").datepicker('getFormattedDate')

        )



        $.ajax({

            url: '{{ route('AnnualReport') }}',

            type: 'POST',

            data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),

                dateYear : $('#my_hidden_input').val(),



            }, 

            success:function(response){



                var obj = JSON.parse(response);

                drawChartCount(obj,$('#my_hidden_input').val());

               

            }

        });



    });









    function drawChartCount(response,year){





        Highcharts.chart('canvasCount', {

            chart: {

                type: 'column'

            },

            title: {

                text: year

            },

            credits: false,

            subtitle: {

                text: ''

            },

            xAxis: {

                categories: [

                 "Quality of services provided",

                "Clarity of procedures",

                "Service Completion Time",

                "Staff knowledge and communication",

                "Waiting area"

                ],

                crosshair: true

            },

            yAxis: {

                min: 0,

                title: {

                    text: 'Count'

                }

            },

            tooltip: {

                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',

                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +

                    '<td style="padding:0"><b>{point.y}</b></td></tr>',

                footerFormat: '</table>',

                shared: true,

                useHTML: true

            },

            plotOptions: {

                column: {

                    pointPadding: 0.2,

                    borderWidth: 0

                }

            },

            series: [{

                name: 'Excellent',

                data: response[5]



            }, {

                name: 'Good',

                data: response[4]



            }, {

                name: 'Average',

                data: response[3]



            }, {

                name: 'Poor',

                data: response[2]



            }, {

                name: 'Very poor',

                data: response[1]



            }]

        });



    }





});

</script>

@endsection