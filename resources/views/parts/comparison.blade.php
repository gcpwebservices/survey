@extends('layouts.app')



@section('content-styles')



    <link rel="stylesheet" href="{{ asset('template/vendor/fontawesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/metisMenu/dist/metisMenu.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/animate.css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap/dist/css/bootstrap.css') }}" />

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">

    <link rel="stylesheet" href="{{ asset('template/vendor/daterangepicker/daterangepicker.css') }}" />



    <style type="text/css">

        .color-line {

            background-color: rgba(128,38,43,0.8) !important;

            background-image: none;

        }



        .highcharts-container, .highcharts-container svg { width: 100% !important; }



    </style>



@endsection



@section('content')



@include('layouts.header')

<div id="wrapper">

    <div class="content">

        <div class="row">

            <div class="col-sm-12 col-lg-8">

                <div class="hpanel">

                    <div class="panel-heading">

                        <div class="panel-tools">

                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>

                        </div>

                        Action

                    </div>

                    <div class="panel-body">

                        <div class="row">

    

                            <div class="col-sm-12 col-md-12">
                                <div class="row">
                                    <div class="col-sm-9 col-md-9">

                                        <strong>Date Filter</strong>

                                        <div class="input-daterange input-group" id="datepicker">

                                            <input type="text" class="form-control" name="start" id="start" autocomplete="off">

                                            <span class="input-group-addon">VS</span>

                                            <input type="text" class="form-control" name="end" id="end" autocomplete="off">

                        
                                        </div>

                                    </div>

                                    <div class="col-sm-3 col-md-3">
                                        <strong>Export Excel</strong>
                                        <div class="form-group">
                                            <form method="POST" enctype="multipart/form-data" action="{{ route('chartComparisonExport') }}">
                                            @csrf
                                                <input type="hidden" name="datefromLabel1" id="datefromLabel1">
                                                <input type="hidden" name="datetoLabel1" id="datetoLabel1">
                                                <input type="hidden" name="datefromLabel2" id="datefromLabel2">
                                                <input type="hidden" name="datetoLabel2" id="datetoLabel2">

                                                <input type="hidden" name="datefrom1" id="datefrom1">
                                                <input type="hidden" name="dateto1" id="dateto1">
                                                <input type="hidden" name="datefrom2" id="datefrom2">
                                                <input type="hidden" name="dateto2" id="dateto2">

                                                <button class="btn btn-default" type="submit"><i class="fa fa-file-excel-o" aria-hidden="true"></i></button>
                                            </form>
                                        </div>
                                    </div> 
                                </div>
                            </div>  

               

                        </div>



                    </div>

                </div>

            </div>



        </div>



        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                    Comparison Chart
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-md-6"><div id="canvasCountPrev" style="width: 108%;"></div></div>
                            <div class="col-sm-6 col-md-6"><div id="canvasCountCur" style="width: 100%;"></div></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@include('layouts.footer')

</div>



@endsection

@section('content-scripts')

<script src="{{ asset('template/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('template/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('template/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('template/vendor/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>

<script src="{{ asset('template/vendor/moment/moment.js') }}"></script>

<script type="text/javascript" src="{{ asset('template/vendor/daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/vendor/daterangepicker/daterangepicker.min.js') }}"></script>

<script src="{{ asset('template/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script src="{{ asset('template/highcharts/highcharts.js') }}"></script>
<script src="{{ asset('template/highcharts/exporting.js') }}"></script>
<script src="{{ asset('template/highcharts/export-data.js') }}"></script>

<script src="{{ asset('template/scripts/homer.js') }}"></script>
<script type="text/javascript">

$(document).ready(function(){



    var fromPrev = moment().subtract(1, 'weeks').startOf('isoWeek').subtract(1,'d').format('YYYY-MM-DD');
    var toPrev = moment().subtract(1, 'weeks').endOf('isoWeek').subtract(1,'d').format('YYYY-MM-DD');
    var fromCur = moment().startOf('isoWeek').subtract(1,'d').format('YYYY-MM-DD');
    var toCur = moment().endOf('isoWeek').subtract(1,'d').format('YYYY-MM-DD');

    CallFrom(fromPrev,toPrev);
    CallTo(fromCur,toCur);

    $('#datefromLabel1').val(moment().subtract(1, 'weeks').startOf('isoWeek').subtract(1,'d').format('LL'));
    $('#datetoLabel1').val(moment().subtract(1, 'weeks').endOf('isoWeek').subtract(1,'d').format('LL'));

    $('#datefromLabel2').val(moment().startOf('isoWeek').subtract(1,'d').format('LL'));
    $('#datetoLabel2').val(moment().endOf('isoWeek').subtract(1,'d').format('LL'));

    $('#datefrom1').val(moment().subtract(1, 'weeks').startOf('isoWeek').subtract(1,'d').format('YYYY-MM-DD'));
    $('#dateto1').val(moment().subtract(1, 'weeks').endOf('isoWeek').subtract(1,'d').format('YYYY-MM-DD'));
    $('#datefrom2').val(moment().startOf('isoWeek').subtract(1,'d').format('YYYY-MM-DD'));
    $('#dateto2').val(moment().endOf('isoWeek').subtract(1,'d').format('YYYY-MM-DD'));


    $('#start').daterangepicker({



        "showWeekNumbers": true,

        "showCustomRangeLabel": false,

        ranges: {

            'Today': [moment(), moment()],

            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

            'Last 7 Days': [moment().subtract(6, 'days'), moment()],

            'Last 30 Days': [moment().subtract(29, 'days'), moment()],

            'This Month': [moment().startOf('month'), moment().endOf('month')],

            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],

            'This Year': [moment().startOf('year'), moment().endOf('year')],

            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],



        },

        "alwaysShowCalendars": true,

        "startDate": moment().subtract(1, 'weeks').startOf('isoWeek').subtract(1,'d'),

        "endDate": moment().subtract(1, 'weeks').endOf('isoWeek').subtract(1,'d')

    }, function(start, end, label) {

        $('#datefromLabel1').val(start.format('LL'));
        $('#datetoLabel1').val(end.format('LL'));

        $('#datefrom1').val(start.format('YYYY-MM-DD'));
        $('#dateto1').val(end.format('YYYY-MM-DD'));

        CallFrom(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));

    });





    $('#end').daterangepicker({

        "showWeekNumbers": true,
        "showCustomRangeLabel": false,

        ranges: {

            'Today': [moment(), moment()],

            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],

            'Last 7 Days': [moment().subtract(6, 'days'), moment()],

            'Last 30 Days': [moment().subtract(29, 'days'), moment()],

            'This Month': [moment().startOf('month'), moment().endOf('month')],

            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],

            'This Year': [moment().startOf('year'), moment().endOf('year')],

            'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],

        },

        "alwaysShowCalendars": true,

        "startDate": moment().startOf('isoWeek').subtract(1,'d'),

        "endDate": moment().endOf('isoWeek').subtract(1,'d')

    }, function(start, end, label) {

        $('#datefromLabel2').val(start.format('LL'));
        $('#datetoLabel2').val(end.format('LL'));

        $('#datefrom2').val(start.format('YYYY-MM-DD'));
        $('#dateto2').val(end.format('YYYY-MM-DD'));

        CallTo(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));

    });



    function CallFrom(fromPrev,toPrev){

        $.ajax({

            url: '{{ route('chartCountComp') }}',

            type: 'POST',

            data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),

                datefrom : fromPrev,
                dateto: toPrev,
           
            }, 

            success:function(response){

                var obj = JSON.parse(response);

                console.log(obj);

                drawChartCountPrev(obj,$('#datefromLabel1').val(),$('#datetoLabel1').val());

            }



        });

    }



    function CallTo(fromPrev,toPrev){

        $.ajax({

            url: '{{ route('chartCountComp') }}',

            type: 'POST',

            data: {

                _token  : $('meta[name="csrf-token"]').attr('content'),

                datefrom : fromPrev,
                dateto: toPrev,

            }, 

            success:function(response){

                var obj = JSON.parse(response);
                drawChartCountCur(obj,$('#datefromLabel2').val(),$('#datetoLabel2').val());

            }



        });

  

    }


    function drawChartCountPrev(response,from,to){

      Highcharts.chart('canvasCountPrev', {
            exporting: { enabled: false },
            chart: {
                type: 'bar',
                height: 600
            },

            title: {
                text: from+" - "+to
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                reversed: true,
                categories: [    
                    "Quality of services provided",
                    "Clarity of procedures",
                    "Service Completion Time",
                    "Staff knowledge and communication",
                    "Waiting area"
                ],
            title: {
                    text: null
                }
            },
            yAxis: {
                reversed: true,
                min: 0,
                title: {
                    text: '',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
        

            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        allowOverlap: true,
                        color: 'black'
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Excellent',
                data: response[5]
            }, {
                name: 'Good',
                data: response[4]
            }, {
                name: 'Average',
                data: response[3]
            }, {
                name: 'Poor',
                data: response[2]
            }, {
                name: 'Very poor',
                data: response[1]
            }]
        });


    }


    function drawChartCountCur(response,from,to){

        Highcharts.chart('canvasCountCur', {
            exporting: { enabled: false },
            chart: {
                type: 'bar',
                height: 600
            },

            title: {
                text: from+" - "+to
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                labels: {
                    enabled: false
                },
                categories: [    
                    "Quality of services provided",
                    "Clarity of procedures",
                    "Service Completion Time",
                    "Staff knowledge and communication",
                    "Waiting area"
                ],
            title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
         

            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true,
                        allowOverlap: true,
                        color: 'black'
                    }
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'bottom',
                x: 0,
                y: 0,
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Excellent',
                data: response[5]
            }, {
                name: 'Good',
                data: response[4]
            }, {
                name: 'Average',
                data: response[3]
            }, {
                name: 'Poor',
                data: response[2]
            }, {
                name: 'Very poor',
                data: response[1]
            }]
        });


    }


});

</script>

@endsection