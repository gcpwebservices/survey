@extends('layouts.app')

@section('content-styles')

    <link rel="stylesheet" href="{{ asset('template/vendor/fontawesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/metisMenu/dist/metisMenu.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/animate.css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap/dist/css/bootstrap.css') }}" />

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">

    <link rel="stylesheet" href="{{ asset('template/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" />

    <style type="text/css">
        .color-line {
            background-color: rgba(128,38,43,0.8) !important;
            background-image: none;
        }
    </style>

@endsection

@section('content')

@include('layouts.header')
<div id="wrapper">

    <div class="content animate-panel">
        @if(session('status'))
        <div class="col-lg-12" id="successMessage">
            <div class="hpanel">
                <div class="panel-body">
                    <div id="" class="alert alert-{{session('status')[0]}}" style="background-color: #dff0d8 !important;color: #3c763d!important; border-color: #d6e9c6 !important;">
                    {{session('status')[1]}}
                    </div>
          
                </div>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-lg-12">
               <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            <a class="closebox"><i class="fa fa-times"></i></a>
                        </div>
                        Question Categories
                    </div>
                    <div class="panel-body">
                        <table id="example2" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Status</th>
                            <th>Active Date</th>
                        </tr>
                        </thead>
                            <tbody>
                                <tr>
                                    <td>Tiger Nixon</td>
                                    <td>System Architect</td>
                                    <td>Edinburgh</td>
                                </tr>
                      
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
      
        </div>

    </div>
@include('layouts.footer')
</div>



@endsection


@section('content-scripts')

<script src="{{ asset('template/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('template/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('template/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('template/vendor/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('template/vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('template/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- DataTables buttons scripts -->
<script src="{{ asset('template/vendor/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('template/vendor/pdfmake/build/vfs_fonts.js') }}"></script>
<script src="{{ asset('template/vendor/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('template/vendor/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('template/vendor/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('template/vendor/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>

<script src="{{ asset('template/vendor/moment/moment.js') }}"></script>

<script src="{{ asset('template/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>



<!-- App scripts -->
<script src="{{ asset('template/scripts/homer.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#example2').dataTable();
        $('#active_from, #active_to').datepicker();

        setTimeout(function() {
            $('#successMessage').fadeOut('fast');
        }, 5000);
        
    });
</script>
@endsection