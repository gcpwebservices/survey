@extends('layouts.app')



@section('content-styles')
    <link rel="stylesheet" href="{{ asset('template/vendor/fontawesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/metisMenu/dist/metisMenu.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/animate.css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap/dist/css/bootstrap.css') }}" />
    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">
    <link rel="stylesheet" href="{{ asset('template/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/daterangepicker/daterangepicker.css') }}" />
    <style type="text/css">
        .color-line {
            background-color: rgba(128,38,43,0.8) !important;
            background-image: none;
        }
    </style>
@endsection

@section('content')

@include('layouts.header')

<div id="wrapper">
<input type="hidden" name="datefrom" id="datefrom">
<input type="hidden" name="dateto" id="dateto">
    <div class="content">
        <div class="row">
            <div class="col-sm-12 col-lg-4">
                <div class="hpanel">
                    <div class="panel-heading">
                        <div class="panel-tools">
                            <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        </div>
                        Action
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="col-sm-12">
                                    <strong>Date Filter</strong>
                                    <div class="input-group">
                                        <input class="form-control datefilter" name="datefilter" type="text" placeholder="">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default datefilter"><i class="fa fa-calendar"></i></button>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="hpanel">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                       <table id="example1" class="table table-striped table-bordered table-hover" width="100%">
                             <thead>
                                <tr>
                                    <th>Fullname</th>
                                    <th>Company</th>
                                    <th>Mobile</th>
                                    <th>Quality of services provided</th>
                        
                                    <th>Clarity of procedures</th>
                                    <th>Service Completion Time</th>
                                    <th>Staff knowledge and communication</th>
                                    <th>Waiting area</th>
                                    <th>Suggestion</th>
                                    <th>DateTime</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer')
</div>



@endsection





@section('content-scripts')


<script src="{{ asset('template/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('template/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('template/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('template/vendor/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>
<script src="{{ asset('template/vendor/moment/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/vendor/daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('template/vendor/daterangepicker/daterangepicker.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('template/vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('template/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<!-- DataTables buttons scripts -->

<script src="{{ asset('template/vendor/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('template/vendor/pdfmake/build/vfs_fonts.js') }}"></script>


<script src="{{ asset('template/vendor/newdatatable/buttons.print.min.js') }}"></script>
<script src="{{ asset('template/vendor/newdatatable/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('template/vendor/newdatatable/jszip.min.js') }}"></script>
<script src="{{ asset('template/vendor/newdatatable/pdfmake.min.js') }}"></script>
<script src="{{ asset('template/vendor/newdatatable/vfs_fonts.js') }}"></script>
<script src="{{ asset('template/vendor/newdatatable/buttons.html5.min.js') }}"></script>


<script src="{{ asset('template/scripts/homer.js') }}"></script>
<script type="text/javascript">

$(document).ready(function(){

    var from = moment().subtract(6, 'days').format('YYYY-MM-DD');
    var to   = moment().format('YYYY-MM-DD');

    $('#datefrom').val(from);
    $('#dateto').val(to);

    $('.datefilter').daterangepicker({

        ranges: {

            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

        },

        "alwaysShowCalendars": true,
        "startDate": moment().subtract(6, 'days'),
        "endDate": moment()
    }, function(start, end, label) {

        $('#datefrom').val(start.format('YYYY-MM-DD'));
        $('#dateto').val(end.format('YYYY-MM-DD'));

        respondentsTables.ajax.reload();

    });


    var respondentsTables = $('#example1').DataTable({

        processing: true,
        serverSide: false,
        ordering: true,
        "order": [[ 9, "desc" ]],
        dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Survey Respondents '+ from +' to '+to,
                exportOptions : {
                    modifier : {
                        search: 'applied',
                        order: 'applied'
                    }
                }
            }
        ],

        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        oLanguage: {
            sProcessing: "<span id='spinner' class='fa fa-spinner fa-2x fa-spin text-blue'></span>"
        },

        ajax: { 
            url: '{{ route('getRespondentsDetails') }}',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            data: function ( d ) {
                d.datefrom = $('#datefrom').val();
                d.dateto = $('#dateto').val();
            }
        },

        "createdRow": function( row, data, dataIndex ) {

            $('td',row).hover(function() {
                $(this).addClass('pointer');
            });

            $('td',row).on('click',function(){
                // $('#idval').val(data.id);
                // window.location.href = '{{ url("admin/accounting") }}'+'/'+data.id+'/transaction-details';
            });

        },

        columns: [

            {data:'respondent',name:'respondent',class:'nowrap'},
            {data:'company',name:'company'},
            {data:'mobile',name:'mobile'},
            {data:'question1',name:'question1'},
            {data:'question2',name:'question2'},
            {data:'question3',name:'question3'},
            {data:'question4',name:'question4'},
            {data:'question5',name:'question5'},
            {data:'suggestion',name:'suggestion'},
            {data:'created_at',name:'created_at'},
          
        ]

    });






});






</script>

@endsection