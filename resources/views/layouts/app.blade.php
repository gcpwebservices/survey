<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" >
    
 	<meta name="apple-mobile-web-app-capable" content="yes">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Raktda Survey</title>
    <link href="{{ asset('images/raktdico.ico') }}" rel="icon">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
@yield('content-styles')
</head>

@yield('content')

@yield('content-scripts')
</html>
