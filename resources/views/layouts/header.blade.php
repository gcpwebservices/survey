<div id="header">

    <div class="color-line">

    </div>

    <div id="logo" class="light-version">

        <span>

            Survey APP

        </span>

    </div>

    <nav role="navigation">

        <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>

        <div class="small-logo">

            <span class="text-primary">Survey APP</span>

        </div>



        <div class="mobile-menu">

            <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse" data-target="#mobile-collapse">

                <i class="fa fa-chevron-down"></i>

            </button>

            <div class="collapse mobile-navbar" id="mobile-collapse">

                <ul class="nav navbar-nav">

                    <li>

                        <a class="" href="{{ url('/logout') }}">Login</a>

                    </li>

                    <li>

                        <a class="" href="{{ url('/logout') }}">Logout</a>

                    </li>

                    <li>

                        <a class="" href="profile.html">Profile</a>

                    </li>

                </ul>

            </div>

        </div>

        <div class="navbar-right">

            <ul class="nav navbar-nav no-borders">

                <li class="dropdown">

                    <a href="{{ url('/logout') }}">

                        <i class="pe-7s-upload pe-rotate-90"></i>

                    </a>

                </li>

            </ul>

        </div>

    </nav>

</div>





<aside id="menu">

    <div id="navigation">

        <div class="profile-picture">

         <img class="img-fluid" alt="Responsive image" width="140" src="{{ asset('images/raktd.png') }}">

            <div class="stats-label text-color">

                <span class="font-extra-bold font-uppercase">{{ Auth::user()->name }}</span>

                <div class="dropdown">

                    <a class="dropdown-toggle" href="#" data-toggle="dropdown">

                        <small class="text-muted">Hello! <b class="caret"></b></small>

                    </a>

                    <ul class="dropdown-menu animated flipInX m-t-xs">

                        <li><a href="{{ url('/logout') }}">Logout</a></li>

                    </ul>

                </div>



            </div>

        </div>



        <ul class="nav" id="side-menu">

            <li><a href="{{ url('/admin/reports') }}">Count & Percentage</a></li>

            <li><a href="{{ url('/admin/comparison') }}">Comparison</a></li>

            <li><a href="{{ url('/admin/annual') }}">Annual</a></li>

            <li><a href="{{ url('/admin/suggestions') }}">Suggestions</a></li>

            <li><a href="{{ url('/admin/respondents') }}">Respondents</a></li>

        </ul>

    </div>

</aside>

