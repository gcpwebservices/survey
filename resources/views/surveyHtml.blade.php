@extends('layouts.app')

@section('content-styles')
    <link rel="stylesheet" href="{{ asset('template/vendor/fontawesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/metisMenu/dist/metisMenu.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/animate.css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap/dist/css/bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/xeditable/bootstrap3-editable/css/bootstrap-editable.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/select2-3.5.2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/select2-bootstrap/select2-bootstrap.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/bootstrap-datepicker-master/dist/css/bootstrap-datepicker3.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/clockpicker/dist/bootstrap-clockpicker.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" />

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">
    <link rel="stylesheet" href="{{ asset('template/styles/style.css') }}">

<style type="text/css">

    html {
      touch-action: manipulation;
    }

    .img-fluid {
        margin-bottom: -25px;
    }

    h5 {
        font-size: 15px !important;
    }

    h4 {
        font-size: 14px !important;
    }

    .color-line {
        background-color: rgba(128,38,43,0.8) !important;
        background-image: none;
        height: 3px;
    }

    .radio {
        touch-action: manipulation;
    }

    .radio label::before {
        touch-action: manipulation;
        transform: scale(1.5, 1.5) !important;
    }

    .radio label::after {
        touch-action: manipulation;
        background-color: rgba(128,38,43,0.8) !important;
        width: 17px !important;
        height: 17px !important;
        left: 0px !important;
        top: 0px !important;
    }

    hr {
        margin-bottom: 0px !important;
        margin-top: 0px !important;
    }

    .p-20 {
        padding: 10px;
    }

    .smiley {
        margin-left: -1em;
    }

    .btn-primary {
        background-color: rgba(128,38,43,0.8) !important;
        border-color: rgba(128,38,43,0.8) !important;
    }

    .stop-scrolling {
      height: 100%;
      overflow: hidden;
    }
    
    .has-error.question h5 {
       color: red;
    }

    .has-error.form-group label {
       color: red;
    }

    label {
        font-weight: normal !important;
    }

    .has-error .form-control {
       border-color: #e4e5e7 !important;
    }



</style>
@endsection

@section('content')
<body id="body">
    <main class="">
        <div id="logo1" class="col-sm-12 col-xs-12 text-center">
            <img class="img-fluid" alt="Responsive image" width="180" src="{{ asset('images/raktd.png') }}">
            <div>
                <h4 style="color:#6d7072;">آراؤكم تهمنا - إدارة التراخيص السياحية و ضمان الجودة</h4>
                <h4 style="color: #6d7072;">We Value your opinions - TLQA Department </h4>
            </div>
            <div class="color-line"></div>
        </div> 
        <div class="container-fluid">
        <form method="POST" action="{{ route('survey.store') }}">
            @csrf
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="row p-20">
                        <div class="col-sm-12">
                            <h5><strong>1. Kindly share your details.</strong></h5>
                        </div>
                    </div>
                </div>
             
                <div class="col-sm-4 col-xs-12">
                    <div class="row p-20">
                   
                        <div class="form-group{{ $errors->has('fullname') ? ' has-error' : '' }}">
                        <label for="name" class="col-sm-4 col-xs-5">Fullname: <span style="color: red;">*</span></label>
                            <div class="col-sm-8 col-xs-7">
                                <input type="text" class="form-control" name="fullname" id="fullname" value="{{ old('fullname') }}" 
                                autocomplete="off">
                            </div>
                        </div>
                 

                    </div>

                </div>

                <div class="col-sm-4 col-xs-12">
                    <div class="row p-20">
                   
                        <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}" >

                        <label for="name" class="col-sm-4 col-xs-5">Company: <span style="color: red;">*</span></label>
                            <div class="col-sm-8 col-xs-7">
                                <input type="text" class="form-control" name="company" id="company" value="{{ old('company') }}" 
                                autocomplete="off">
                            </div>
                        </div>
                 

                    </div>

                </div>

                <div class="col-sm-4 col-xs-12">
                    <div class="row p-20">
                   
                        <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                        <label for="name" class="col-sm-4 col-xs-5">Mobile: <span style="color: red;">*</span></label>
                            <div class="col-sm-8 col-xs-7">
                                <input type="text" class="form-control" name="mobile" id="mobile" placeholder="+971" value="{{ old('mobile') }}" autocomplete="off">
                            </div>
                        </div>
                 

                    </div>

                </div>

            </div>

            <hr>

            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="row p-20">
                        <div class="col-xs-7">
                            <h5><strong>2. Please indicate if you agree or disagree with the following statements.</strong></h5>
                        </div>
                        <div class="col-xs-1">
                            <img src="{{ asset('images/five.png') }}" width="45" class="smiley">
                        </div>
                        <div class="col-xs-1">
                            <img src="{{ asset('images/four.png') }}" width="45"  class="smiley">
                        </div>
                        <div class="col-xs-1">
                            <img src="{{ asset('images/three.png') }}" width="45"  class="smiley">
                        </div>
                        <div class="col-xs-1">
                            <img src="{{ asset('images/two.png') }}" width="45"  class="smiley">
                        </div>
                        <div class="col-xs-1">
                            <img src="{{ asset('images/one.png') }}" width="45"  class="smiley">
                        </div>
                    </div>
                    
                    <div class="p-20">
                        <div class="row question{{ $errors->has('q1') ? ' has-error' : '' }}">

                             <div class="col-xs-7">
                                <h5> Quality of services provided / جودة الخدمات المقدمة  <span style="color: red;">*</span> </h5>
                                
                             </div>
                              <div class="col-xs-1">
                                    <div class="radio">
                                        <input type="radio" id="singleRadio1" {{ old('q5') == 5 ? 'checked' : '' }} value="5" name="q1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1" {{ old('q5') == 4 ? 'checked' : '' }} value="4" name="q1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1" {{ old('q5') == 3 ? 'checked' : '' }} value="3" name="q1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1" {{ old('q5') == 2 ? 'checked' : '' }} value="2" name="q1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1" {{ old('q5') == 1 ? 'checked' : '' }} value="1" name="q1" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                        </div>
                        <hr>
                        <div class="row question{{ $errors->has('q2') ? ' has-error' : '' }}">
                             <div class="col-xs-7">
                                <h5> Clarity of procedures / وضوح الإجراءات       <span style="color: red;">*</span> </h5>
                                
                             </div>
                              <div class="col-xs-1">
                                    <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q2') == 5 ? 'checked' : '' }} value="5" name="q2" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q2') == 4 ? 'checked' : '' }} value="4" name="q2" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q2') == 3 ? 'checked' : '' }} value="3" name="q2" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q2') == 2 ? 'checked' : '' }} value="2" name="q2" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q2') == 1 ? 'checked' : '' }} value="1" name="q2" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                        </div>
                        <hr>
                        <div class="row question{{ $errors->has('q3') ? ' has-error' : '' }}">
                             <div class="col-xs-7">
                                <h5> Service Completion Time / مدة إنجاز المعاملة       <span style="color: red;">*</span> </h5>
                                
                             </div>
                              <div class="col-xs-1">
                                    <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q3') == 5 ? 'checked' : '' }} value="5" name="q3" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q3') == 4 ? 'checked' : '' }} value="4" name="q3" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q3') == 3 ? 'checked' : '' }} value="3" name="q3" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q3') == 2 ? 'checked' : '' }} value="2" name="q3" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q3') == 1 ? 'checked' : '' }} value="1" name="q3" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                        </div>
                        <hr>
                        <div class="row question{{ $errors->has('q4') ? ' has-error' : '' }}">
                             <div class="col-xs-7">
                                <h5> Staff knowledge and communication / معرفة الموظفين و جودة التعامل       <span style="color: red;">*</span> </h5>
                                
                             </div>
                              <div class="col-xs-1">
                                    <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q4') == 5 ? 'checked' : '' }} value="5" name="q4" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q4') == 4 ? 'checked' : '' }} value="4" name="q4" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q4') == 3 ? 'checked' : '' }} value="3" name="q4" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q4') == 2 ? 'checked' : '' }} value="2" name="q4" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1" {{ old('q4') == 1 ? 'checked' : '' }} value="1" name="q4" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                        </div>
                        <hr>
                        <div class="row question{{ $errors->has('q5') ? ' has-error' : '' }}">
                             <div class="col-xs-7">
                                <h5> Waiting area / قاعة الانتظار       <span style="color: red;">*</span> </h5>
                                
                             </div>
                              <div class="col-xs-1">
                                    <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q5') == 5 ? 'checked' : '' }} value="5" name="q5" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q5') == 4 ? 'checked' : '' }} value="4" name="q5" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q5') == 3 ? 'checked' : '' }} value="3" name="q5" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q5') == 2 ? 'checked' : '' }} value="2" name="q5" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                              <div class="col-xs-1">
                                        <div class="radio">
                                        <input type="radio" id="singleRadio1"  {{ old('q5') == 1 ? 'checked' : '' }} value="1" name="q5" aria-label="Single radio One">
                                        <label></label>
                                    </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
              
            <hr>
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                     <div class="row p-20">
                        <div class="col-sm-12">
                            <h5><strong>3. Kindly share with us your suggestions.</strong></h5>
                        </div>

                        <div class="col-sm-12 col-xs-12">
                           <input type="text" placeholder="" name="suggestions" class="form-control input-lg" value="{{ old('suggestions') }}" autocomplete="off">
                        </div>
                    </div>
                  
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group pull-right p-20">
                        <button class="btn btn-primary btn-lg" type="submit">SUBMIT</button>
                    </div>  
                </div>
            </div>

        </div>
        </form>
    </main>
</body>

@endsection

@section('content-scripts')
<script src="{{ asset('template/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('template/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('template/vendor/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>
<script src="{{ asset('template/vendor/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>
<script src="{{ asset('template/vendor/moment/moment.js') }}"></script>
<script src="{{ asset('template/vendor/xeditable/bootstrap3-editable/js/bootstrap-editable.min.js') }}"></script>
<script src="{{ asset('template/vendor/select2-3.5.2/select2.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('template/vendor/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('template/vendor/sparkline/index.js') }}"></script>

<script type="text/javascript">

// $('body').addClass('stop-scrolling');
// $('body').bind('touchmove', function(e){e.preventDefault()})

</script>
@endsection