@extends('layouts.app')
@section('content-styles')
    <link rel="stylesheet" href="{{ asset('/template/vendor/fontawesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/vendor/metisMenu/dist/metisMenu.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/vendor/animate.css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/vendor/bootstrap/dist/css/bootstrap.css') }}" />

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('/template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/styles/style.css') }}">
    <style type="text/css">
        .radio label, .checkbox label {
            padding-left: 20px;
        }
        .form-check-input {
            margin-left: 0px !important;
        }
        .color-line {
            background-color: rgba(128,38,43,0.8) !important;
            background-image: none;
        }

        .btn-success {
            background-color: rgba(128,38,43,0.8) !important;
            border-color: rgba(128,38,43,0.8) !important;
        }
    </style>
@endsection
@section('content')
<body class="blank">

<div class="register-container">
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <div class="text-center m-b-md">
               <div class="text-center m-b-md">
                    <img class="img-fluid" alt="Responsive image" width="200" src="{{ asset('images/raktd.png') }}">
                    <h3>REGISTRATION</h3>
                </div>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-12 col-xs-12 col-sm-12">
                                    <label>Fullname</label>
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus autocomplete="off">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <label>Email Address</label>
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required  autocomplete="off">

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label>Password</label>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label>Confirm Password</label>

                             
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                             
                                </div>
                    
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">Register</button>
                                <a href="{{ url('login') }}" class="btn btn-default">Back to Login</a>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
           © Ras Al Khaimah Tourism Development Authority
        </div>
    </div>
</div>



</body>
@endsection
