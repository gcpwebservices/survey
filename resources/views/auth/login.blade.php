@extends('layouts.app')
@section('content-styles')
    <link rel="stylesheet" href="{{ asset('/template/vendor/fontawesome/css/font-awesome.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/vendor/metisMenu/dist/metisMenu.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/vendor/animate.css/animate.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/vendor/bootstrap/dist/css/bootstrap.css') }}" />

    <!-- App styles -->
    <link rel="stylesheet" href="{{ asset('/template/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/fonts/pe-icon-7-stroke/css/helper.css') }}" />
    <link rel="stylesheet" href="{{ asset('/template/styles/style.css') }}">
    <style type="text/css">
        .radio label, .checkbox label {
            padding-left: 20px;
        }
        .form-check-input {
            margin-left: 0px !important;
        }
        .color-line {
            background-color: rgba(128,38,43,0.8) !important;
            background-image: none;
        }

        .btn-success {
            background-color: rgba(128,38,43,0.8) !important;
            border-color: rgba(128,38,43,0.8) !important;
        }
    </style>
@endsection

@section('content')
<body class="blank">

<div class="color-line"></div>

<div class="login-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <img class="img-fluid" alt="Responsive image" width="200" src="{{ asset('images/raktd.png') }}">
                <h3>PLEASE LOGIN</h3>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                        <form id="loginForm" method="POST" action="{{ route('login') }}">
                        @csrf
                            <div class="form-group">
                                <label class="control-label" for="username">Username</label>
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"    value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                <span class="help-block small">Your unique email to app</span>
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" 
                                name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                <span class="help-block small">Your strong password</span>
                            </div>
                            <div class="checkbox">
                               <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success btn-block">
                                {{ __('Login') }}
                            </button>
                            <a class="btn btn-default btn-block" href="{{ url('register') }}">Register</a>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
        <!--     <strong>HOMER</strong> - AngularJS Responsive WebApp <br/> 2015 Copyright Company Name -->
        </div>
    </div>
</div>


</body>
@endsection
