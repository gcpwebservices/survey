@extends('layouts.app')

@section('content-styles')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link href="https://surveyjs.azureedge.net/1.0.59/survey.css" type="text/css" rel="stylesheet"/>
<style type="text/css">

    @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation:portrait) {
      body {
        -webkit-transform: rotate(90deg);
        width: 100%;
        height: 100%;
        overflow: hidden;
        position: absolute;
        top: 0;
        left: 0;
      }
    }

    th {
      
        line-height: 4.3em !important;
        position: relative !important;
        left: 20px;
        -webkit-transform: scale(3);
    }

    .sv_main .sv_container .sv_body .sv_p_root table.sv_q_matrix td {
        min-width: 1em !important;
    }

    .sv_main .sv_container .sv_body .sv_p_root table.sv_q_matrix td .sv_q_m_label {
        font-size: 18px;
        adding: 0em 1em 1.5em 1em !important; 
    }

    .sv_main .sv_container .sv_body .sv_p_root .sv_q {
        overflow: hidden !important;
    }

    .sv_complete_btn {
        font-size: 30px !important;
    }

    h5 {
        font-size: 13px !important;
    }

    h4 {
        font-size: 14px !important;
    }

    .sv_main .sv_container .sv_body .sv_p_root .sv_q {
        padding: 0.5em 1em 0 1em !important;
    }
    
    .sv_main .sv_container .sv_body .sv_p_root table td {
        padding: 5px !important;
    }

    .sv_main .sv_container .sv_body {
        padding: 0px !important;
    }

   .sv_main .sv_container .sv_body .sv_p_root table.sv_q_matrix td .sv_q_m_label {
     right: 10px !important;
        position: relative !important;
   }

   .img-fluid {
        margin-bottom: -14px;
    }
    fieldset {
        position: relative !important;
        bottom: 40px !important;

    }

    .highcharts-container {
        width:100% !important;
        height:100% !important;
    }

    #highcharts-b35d03w-0 {
      width:100% !important;
        height:100% !important; 
    }

    @media screen and (min-width: 1024px) and (max-width: 1920px) {
      th {
        position: relative !important;
        left: 50px;
      }
    }

    @media screen and (max-width: 1024px) {
      th {
        position: relative !important;
        left: 20px !important;
      }
    }

    .sv_main .sv_q_erbox {
        border: 1px solid transparent !important;
        background-color: transparent !important;
    }

    .sv_main.sv_default_css .sv_p_root > .sv_row {

        background-color: #FFFFFF !important;
    }

    .sv_main .sv_select_wrapper:before {
        background-color: rgba(128,38,43,0.8) !important;
    }

    .sv_main .sv_container .sv_body .sv_p_root .sv_q .sv_select_wrapper {
        width: 25% !important;
    }

    textarea {
        height: 55px !important;
    }
</style>
@endsection

@section('content')
<body id="body">
    <main class="">
        <div id="logo1" class="col-sm-12 text-center">
            <img class="img-fluid" alt="Responsive image" width="180" src="{{ asset('images/raktd.png') }}">
            <div>
                <h4 style="color:#6d7072;">آراؤكم تهمنا - إدارة التراخيص السياحية و ضمان الجودة</h4>
                <h4 style="color: #6d7072;">We Value your opinions - TLQA Department </h4>
            </div>
        </div> 
        <div class="container-fluid">
            <div class="col-sm-12" id="surveyElement"></div>
            <!--     <div id="surveyResult"></div> -->
        </div>
    </main>
</body>

@endsection

@section('content-scripts')
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="https://unpkg.com/jquery"></script>
<script src="https://surveyjs.azureedge.net/1.0.59/survey.jquery.js"></script>

<script type="text/javascript">

Survey
    .StylesManager
    .applyTheme("default");

var json = {

    questions: [


        {
            type: "multipletext",
            name: "respondentdetails",
            title: "Kindly share your details",
   
            colCount: 3,
            items: [
                {
                    name: "respondent",
                    title: "Fullname:",
                             isRequired: true,

                }, {
                    name: "company",
                    title: "Company:",
                             isRequired: true,
 
                }, {
                    name: "mobile",
                    title: "Mobile:",
         isRequired: true,
                },
            ]
        },



        {
            type: "matrix",
            name: "Quality",
            title: "Please indicate if you agree or disagree with the following statements",
            isAllRowRequired: true,

            columns: [
                {
                    value: 5,
                    text: "😃" 
                }, {
                    value: 4,
                    text: "😊"
                }, {
                    value: 3,
                    text: "😐"
                }, {
                    value: 2,
                    text: "😒"
                }, {
                    value: 1,
                    text: "😩"
                }
            ],
            rows: [
                <?php foreach($survey as $list ){ ?>
                    <?php foreach($list->getQuestions as $data ){ ?> 
                    {
                        value: <?php echo $data->id; ?>,
                        text: <?php echo "'".$data->question."'"; ?>,
                    },  
                    <?php }?> 
                <?php }?>
            ]
        },

        { type: "comment", name: "suggestions", title:"Kindly share with us your suggestions", },
    ]
};

window.survey = new Survey.Model(json);

survey
    .onComplete
    .add(function (result) {
        console.log(result.data);

        $.ajax({
            url: '{{ route('getSurvey') }}',
            type: 'POST',
            data: {
                 _token  : $('meta[name="csrf-token"]').attr('content'),
                 arr: result.data
            },
            success: function(res){

                setTimeout(function(){ 
                    location.reload();
                }, 3000);
        }});

        // document
        //     .querySelector('#surveyResult')
        //     .innerHTML = "result: " + JSON.stringify(result.data);
    });

$("#surveyElement").Survey({model: survey});


</script>
@endsection